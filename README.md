# SQL Data Access

Sql Data Access is a library of classes and scripts for creating, reading from and manipulating SQLServer Databases.

## Installation

Install Visual Studio to view and run the DataAccessWithSqlScripts project. <br> 
Install Microsoft SQLServer Management Studio to explore the databases created.

## Usage

### DataAccessWithSQLClient
To run the test methods in DataAccessWithSQLClient, you must first have the Chinook database created in SSMS. <br> 
In Utils/ConnectionHelper.cs, replace the string value of sqlConnectionStringBuilder. <br> 
DataSource with the name of your database instance. Run the program to view test results. <br> 

### SuperheroesSQLScripts
Open the files in SSMS and run each SQL script in the correct order to create and manipulate the Superheroes database.

## Contributing
Erik Alstad  <br> 
Eirik Torp