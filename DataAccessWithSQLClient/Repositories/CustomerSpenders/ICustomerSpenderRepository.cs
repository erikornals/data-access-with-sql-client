﻿using DataAccessWithSQLClient.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessWithSQLClient.Repositories.CustomerSpenders
{
    internal interface ICustomerSpenderRepository : ICRUDRepository<CustomerSpender, int>
    {
    }
}
