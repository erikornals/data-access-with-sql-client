﻿using DataAccessWithSQLClient.Models;
using DataAccessWithSQLClient.Utils;
using Microsoft.Data.SqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessWithSQLClient.Repositories.CustomerSpenders
{
    internal class CustomerSpenderRepositoryImpl : ICustomerSpenderRepository
    {

        private readonly string _connectionString;

        public CustomerSpenderRepositoryImpl()
        {
            _connectionString = ConnectionHelper.GetConnectionString();
        }


        public void Add(CustomerSpender obj)
        {
            throw new NotImplementedException();
        }

        public void Delete(CustomerSpender obj)
        {
            throw new NotImplementedException();
        }

        public List<CustomerSpender> GetAll()
        {
            List<CustomerSpender> custSpendList = new List<CustomerSpender>();
            string query = " SELECT c.CustomerId, FirstName, LastName, TotalSpend " +
                "FROM (SELECT CustomerId, sum(Total) AS TotalSpend FROM Invoice GROUP BY CustomerId) s " +
                "LEFT JOIN Customer c on s.CustomerId=c.CustomerId " +
                "ORDER BY TotalSpend DESC";

            try
            {
                //Connect
                using SqlConnection conn = new SqlConnection(_connectionString);
                conn.Open();
                //Make a command
                using SqlCommand cmd = new SqlCommand(query, conn);
                //Reader
                using SqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    //Handle result

                    int id = reader.GetInt32(0);
                    string firstName = reader.SafeGetString(1);
                    string lastName = reader.SafeGetString(2);
                    double totalSpend = (double)reader.GetDecimal(3);
                    CustomerSpender temp = new CustomerSpender(id, firstName, lastName, totalSpend);
                    custSpendList.Add(temp);
                }
            }
            catch (SqlException exc)
            {
                throw exc;
            }
            return custSpendList;
        }

        public CustomerSpender GetById(int id)
        {
            throw new NotImplementedException();
        }

        public void Update(CustomerSpender obj)
        {
            throw new NotImplementedException();
        }
    }
}
