﻿using DataAccessWithSQLClient.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessWithSQLClient.Repositories.CustomerGenres
{
    internal interface ICustomerGenreRepository : ICRUDRepository<CustomerGenre, int>
    {

    }
}
