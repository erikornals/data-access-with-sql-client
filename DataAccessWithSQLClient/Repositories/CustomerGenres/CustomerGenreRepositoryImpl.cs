﻿using DataAccessWithSQLClient.Models;
using DataAccessWithSQLClient.Utils;
using Microsoft.Data.SqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessWithSQLClient.Repositories.CustomerGenres
{
    internal class CustomerGenreRepositoryImpl : ICustomerGenreRepository
    {

        private readonly string _connectionString;

        public CustomerGenreRepositoryImpl()
        {
            _connectionString = ConnectionHelper.GetConnectionString();
        }


        public void Add(CustomerGenre obj)
        {
            throw new NotImplementedException();
        }

        public void Delete(CustomerGenre obj)
        {
            throw new NotImplementedException();
        }

        public List<CustomerGenre> GetAll()
        {
            throw new NotImplementedException();
        }

        public CustomerGenre GetById(int id)
        {
            List<string> genres = new List<string>();

            using SqlConnection connection = new SqlConnection(_connectionString);
            connection.Open();
            string sql = "WITH Customer_Genres (CustomerId, FirstName, LastName, Quantity, GenreName) " +
                "AS (" +
                    "SELECT c.CustomerId, c.FirstName, c.LastName, il.Quantity, g.[Name] AS GenreName " +
                    "FROM Customer c " +
                    "LEFT JOIN Invoice i ON c.CustomerId=i.CustomerId " +
                    "LEFT JOIN InvoiceLine il ON il.InvoiceId=i.InvoiceId " +
                    "LEFT JOIN Track t ON il.TrackId=t.TrackId " +
                    "LEFT JOIN Genre g ON t.GenreId=g.GenreId" +
                "), Customer_GenreCount AS (" +
                    "SELECT *, count(*)*Quantity AS GenreCount " +
                    "FROM Customer_Genres " +
                    "WHERE GenreName IS NOT null " +
                    "GROUP BY CustomerId, FirstName, LastName, Quantity, GenreName" +
                ") " +
                "SELECT CustomerId, FirstName, LastName, GenreName, GenreCount " +
                "FROM Customer_GenreCount t1 " +
                "WHERE GenreCount = (" +
                    "SELECT max(GenreCount) " +
                    "FROM Customer_GenreCount t2 " +
                    "WHERE t1.CustomerId = t2.CustomerId" +
                ") " +
                "AND CustomerId = @Id";

            using SqlCommand command = new SqlCommand(sql, connection);
            command.Parameters.AddWithValue("@Id", id);

            using SqlDataReader reader = command.ExecuteReader();

            if (!reader.Read())
            {
                throw new Exception("No customer exists with that ID");
            }
            string firstName = reader.SafeGetString(1);
            string lastName = reader.SafeGetString(2);
            genres.Add(reader.SafeGetString(3));
            while(reader.Read()) 
            {
                genres.Add(reader.SafeGetString(3)); 
            }
            return new CustomerGenre(id, firstName, lastName, genres);
        }

        public void Update(CustomerGenre obj)
        {
            throw new NotImplementedException();
        }
    }
}
