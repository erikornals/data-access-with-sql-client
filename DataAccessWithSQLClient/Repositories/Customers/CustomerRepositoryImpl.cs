﻿using DataAccessWithSQLClient.Models;
using DataAccessWithSQLClient.Utils;
using Microsoft.Data.SqlClient;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessWithSQLClient.Repositories.Customers
{
    public class CustomerRepositoryImpl : ICustomerRepository
    {
        private readonly string _connectionString;

        public CustomerRepositoryImpl()
        {
            _connectionString = ConnectionHelper.GetConnectionString();
        }

        public List<Customer> GetAll()
        {
            List<Customer> custList = new List<Customer>();
            string query = "SELECT CustomerId, FirstName, LastName, Country, PostalCode, Phone, Email " +
                "FROM Customer";
            try
            {
                //Connect
                using SqlConnection conn = new SqlConnection(_connectionString);
                conn.Open();
                //Make a command
                using SqlCommand cmd = new SqlCommand(query, conn);
                //Reader
                using SqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    //Handle result
                    int id = reader.GetInt32(0);
                    string firstName = reader.SafeGetString(1);
                    string lastName = reader.SafeGetString(2);
                    string country = reader.SafeGetString(3);
                    string postalCode = reader.SafeGetString(4);
                    string phone = reader.SafeGetString(5);
                    string email = reader.SafeGetString(6);
                    Customer temp = new Customer(id, firstName, lastName, country, postalCode, phone, email);
                    custList.Add(temp);
                }
            }
            catch (SqlException exc)
            {
                throw exc;
            }
            return custList;
        }

        public Customer GetById(int id)
        {
            Customer customer;

            using SqlConnection connection = new SqlConnection(_connectionString);
            connection.Open();
            string sql = "SELECT CustomerId, FirstName, LastName, Country, PostalCode, Phone, Email " +
                "FROM Customer " +
                "WHERE CustomerId=@Id;";

            using SqlCommand command = new SqlCommand(sql, connection);
            command.Parameters.AddWithValue("@Id", id);
                
            using SqlDataReader reader = command.ExecuteReader();

            if (!reader.Read())
            {
                throw new Exception("No customer exists with that ID");
            }
            string firstName = reader.SafeGetString(1);
            string lastName = reader.SafeGetString(2);
            string country = reader.SafeGetString(3);
            string postalCode = reader.SafeGetString(4);
            string phone = reader.SafeGetString(5);
            string email = reader.SafeGetString(6);
            customer = new Customer(id, firstName, lastName, country, postalCode, phone, email);
            return customer;
        }

        public Customer GetByName(string name)
        {
            Customer customer;
            string firstNameInput = name.Split(" ")[0];
            string lastNameInput = name.Split(" ")[1];

            using SqlConnection connection = new SqlConnection(_connectionString);
            connection.Open();
            string sql = "SELECT CustomerId, FirstName, LastName, Country, PostalCode, Phone, Email " +
                "FROM Customer " +
                "Where FirstName LIKE @FirstName AND LastName LIKE @LastName;";

            using SqlCommand cmd = new SqlCommand(sql, connection);
            cmd.Parameters.AddWithValue("@FirstName", firstNameInput + "%");
            cmd.Parameters.AddWithValue("@LastName", lastNameInput + "%");

            using SqlDataReader reader = cmd.ExecuteReader();

            if (!reader.Read())
            {
                throw new Exception("No customer exists with that name");
            }
            int id = reader.GetInt32(0);
            string firstName = reader.SafeGetString(1);
            string lastName = reader.SafeGetString(2);
            string country = reader.SafeGetString(3);
            string postalCode = reader.SafeGetString(4);
            string phone = reader.SafeGetString(5);
            string email = reader.SafeGetString(6);
            customer = new Customer(id, firstName, lastName, country, postalCode, phone, email);
            return customer;
        }

        public List<Customer> GetPage(int pageNumber, int pageSize)
        {
            List<Customer> custList = new List<Customer>();
            string query = "SELECT CustomerId, FirstName, LastName, Country, PostalCode, Phone, Email " +
                "FROM Customer ORDER BY CustomerId " +
                "OFFSET @Offset ROWS FETCH NEXT @Limit ROWS ONLY";
            try
            {
                //Connect
                using SqlConnection conn = new SqlConnection(_connectionString);
                conn.Open();
                //Make a command
                using SqlCommand cmd = new SqlCommand(query, conn);
                cmd.Parameters.AddWithValue("@Offset", (pageNumber-1)*pageSize);
                cmd.Parameters.AddWithValue("@Limit", pageSize);
                //Reader
                using SqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    //Handle result
                    int id = reader.GetInt32(0);
                    string firstName = reader.SafeGetString(1);
                    string lastName = reader.SafeGetString(2);
                    string country = reader.SafeGetString(3);
                    string postalCode = reader.SafeGetString(4);
                    string phone = reader.SafeGetString(5);
                    string email = reader.SafeGetString(6);
                    Customer temp = new Customer(id, firstName, lastName, country, postalCode, phone, email);

                    custList.Add(temp);
                }
            }
            catch (SqlException exc)
            {
                throw exc;
            }
            return custList;
        }

        public void Add(Customer customer)
        {
            string query = "INSERT INTO Customer (FirstName, LastName, Country, PostalCode, Phone, Email) " +
                "VALUES (@FirstName, @LastName, @Country, @PostalCode, @Phone, @Email)";
            try
            {
                //Connect
                using SqlConnection conn = new SqlConnection(_connectionString);
                conn.Open();
                //Make a command
                using SqlCommand cmd = new SqlCommand(query, conn);
                cmd.Parameters.AddWithValue("@FirstName", customer.FirstName);
                cmd.Parameters.AddWithValue("@LastName", customer.LastName);
                cmd.Parameters.AddWithValue("@Country", customer.Country);
                cmd.Parameters.AddWithValue("@PostalCode", customer.PostalCode);
                cmd.Parameters.AddWithValue("@Phone", customer.Phone);
                cmd.Parameters.AddWithValue("@Email", customer.Email);
                //Execute command
                cmd.ExecuteNonQuery();
            }
            catch (SqlException exc)
            {
                throw exc;
            }
        }

        public void Update(Customer customer)
        {
            string query = "UPDATE Customer SET FirstName = @FirstName, LastName = @LastName, " +
                "Country = @Country, PostalCode = @PostalCode, Phone = @Phone, Email = @Email " +
                "WHERE CustomerId = @Id";
            try
            {
                //Connect
                using SqlConnection conn = new SqlConnection(_connectionString);
                conn.Open();
                //Make a command
                using SqlCommand cmd = new SqlCommand(query, conn);
                cmd.Parameters.AddWithValue("@FirstName", customer.FirstName);
                cmd.Parameters.AddWithValue("@LastName", customer.LastName);
                cmd.Parameters.AddWithValue("@Country", customer.Country);
                cmd.Parameters.AddWithValue("@PostalCode", customer.PostalCode);
                cmd.Parameters.AddWithValue("@Phone", customer.Phone);
                cmd.Parameters.AddWithValue("@Email", customer.Email);
                cmd.Parameters.AddWithValue("@Id", customer.Id);
                //Execute command
                cmd.ExecuteNonQuery();
            }
            catch (SqlException exc)
            {
                throw exc;
            }
        }

        public void Delete(Customer obj)
        {
            throw new NotImplementedException();
        }

    }
}
