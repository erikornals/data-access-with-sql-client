﻿using DataAccessWithSQLClient.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessWithSQLClient.Repositories.Customers
{
    internal interface ICustomerRepository : ICRUDRepository<Customer, int>
    {
        /// <summary>
        /// Retrieves a particular Customer from the database by its Name.
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public Customer GetByName(string name);

        /// <summary>
        /// Retrieves a subset of Customer instances by page number and page size.
        /// </summary>
        /// <param name="pageNumber"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        public List<Customer> GetPage(int pageNumber, int pageSize);

    }
}
