﻿using DataAccessWithSQLClient.Models;
using DataAccessWithSQLClient.Utils;
using Microsoft.Data.SqlClient;
using System;
using System.Collections.Generic;
using System.Diagnostics.Metrics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessWithSQLClient.Repositories.Countries
{
    internal class CustomerCountryRepositoryImpl : ICustomerCountryRepository
    {

        private readonly string _connectionString;

        public CustomerCountryRepositoryImpl()
        {
            _connectionString = ConnectionHelper.GetConnectionString();
        }


        public void Add(CustomerCountry obj)
        {
            throw new NotImplementedException();
        }

        public void Delete(CustomerCountry obj)
        {
            throw new NotImplementedException();
        }

        public List<CustomerCountry> GetAll()
        {
            List<CustomerCountry> customerCountryList = new List<CustomerCountry>();
            string query = "SELECT Country, COUNT(CustomerId) " +
                "FROM Customer " +
                "GROUP BY Country " +
                "ORDER BY COUNT(CustomerId) DESC";
            try
            {
                //Connect
                using SqlConnection conn = new SqlConnection(_connectionString);
                conn.Open();
                //Make a command
                using SqlCommand cmd = new SqlCommand(query, conn);
                //Reader
                using SqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    string country = reader.SafeGetString(0);
                    int count = reader.GetInt32(1);
                    //Handle result
                    CustomerCountry temp = new CustomerCountry(country, count);
                    customerCountryList.Add(temp);
                }
            }
            catch (SqlException exc)
            {
                throw exc;
            }
            return customerCountryList;
        }

        public CustomerCountry GetById(string id)
        {
            throw new NotImplementedException();
        }

        public void Update(CustomerCountry obj)
        {
            throw new NotImplementedException();
        }
    }
}
