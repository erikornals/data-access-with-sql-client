﻿using DataAccessWithSQLClient.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessWithSQLClient.Repositories.Countries
{
    internal interface ICustomerCountryRepository : ICRUDRepository<CustomerCountry, string>
    {
    }
}
