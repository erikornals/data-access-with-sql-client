﻿using DataAccessWithSQLClient.Models;
using DataAccessWithSQLClient.Repositories.Countries;
using DataAccessWithSQLClient.Repositories.CustomerGenres;
using DataAccessWithSQLClient.Repositories.Customers;
using DataAccessWithSQLClient.Repositories.CustomerSpenders;
using Microsoft.Identity.Client;
using System.Diagnostics.Metrics;
using System.Numerics;
using System.Text;

namespace DataAccessWithSQLClient
{
    internal class Program
    {
        static void Main(string[] args)
        {
            ICustomerRepository customerRepo= new CustomerRepositoryImpl();
            ICustomerCountryRepository customerCountryRepo = new CustomerCountryRepositoryImpl();
            ICustomerGenreRepository customerGenreRepo = new CustomerGenreRepositoryImpl();
            ICustomerSpenderRepository customerSpenderRepo = new CustomerSpenderRepositoryImpl();

            Console.WriteLine("\n-----Select all Customers-----");
            TestSelectAll(customerRepo);
            Console.WriteLine("\n-----Select Customer with ID 5-----");
            TestSelectById(customerRepo, 5);
            Console.WriteLine("\n-----Select Customer with 'Tim Go'-----");
            TestSelectByName(customerRepo, "Tim Go");
            Console.WriteLine("\n-----Select page 2 of Customers-----");
            TestSelectCustomerPage(customerRepo, 2, 5);
            Console.WriteLine("\n-----Insert new Customer-----");
            TestInsert(customerRepo);
            Console.WriteLine("\n-----Update Customer-----");
            TestUpdate(customerRepo);

            Console.WriteLine("\n-----Print Countries by number of customers-----");
            TestSumByCountry(customerCountryRepo);

            Console.WriteLine("\n-----Print Customers by total invoice-----");
            TestCustomersBySpend(customerSpenderRepo);

            Console.WriteLine("\n-----Print Favourite Genre-----");
            TestFavouriteGenre(customerGenreRepo);

        }

        static void TestSelectAll(ICustomerRepository repository)
        {
            PrintCustomers(repository.GetAll());
        }

        static void TestSelectById(ICustomerRepository repository, int id)
        {
            PrintCustomer(repository.GetById(id));
        }
        
        static void TestSelectByName(ICustomerRepository repository, string name)
        {
            PrintCustomer(repository.GetByName(name));
        }

        static void TestSelectCustomerPage(ICustomerRepository repository, int page, int pageSize)
        {
            PrintCustomers(repository.GetPage(page, pageSize));
        }

        static void TestInsert(ICustomerRepository repository)
        {
            Customer test = new Customer(
                61,
                "Yo Yo",
                "Ma",
                "Norway",
                "1356",
                "24952365",
                "@me"
            );
            repository.Add(test);

            Customer result = repository.GetById(61);
            if (result.FirstName == "Yo Yo")
            {
                Console.WriteLine("Yo Yo Ma inserted!");
            }
            else
            {
                Console.WriteLine("Insert failed.");
            }
        }

        static void TestUpdate(ICustomerRepository repository)
        {
            Customer customer = repository.GetById(60);
            Customer updated = new Customer(
                61,
                "Yo Yo",
                "Ma",
                "Deutchland",
                "1356",
                "24952365",
                "@me"
            );
            repository.Update(updated);

            Customer result = repository.GetById(60);
            if (result.Country == "Deutchland")
            {
                Console.WriteLine("Yo Yo Ma updated!");
            }
            else
            {
                Console.WriteLine("Update failed.");
            }
        }

        static void PrintCustomers(IEnumerable<Customer> customers)
        {
            foreach (Customer customer in customers)
            {
                PrintCustomer(customer);
            }
        }

        static void PrintCustomer(Customer customer)
        {
            Console.WriteLine($"\n{customer.Id} {customer.FirstName} {customer.LastName} {customer.Email}");
        }

        static void TestSumByCountry(ICustomerCountryRepository repository)
        {
            PrintCustomerCountries(repository.GetAll());
        }

        static void PrintCustomerCountries(IEnumerable<CustomerCountry> customerCountries)
        {
            foreach (CustomerCountry customerCountry in customerCountries)
            {
                PrintCustomerCountry(customerCountry);
            }
        }
        
        private static void PrintCustomerCountry(CustomerCountry customerCountry)
        {
            Console.WriteLine($"\n{customerCountry.Country} {customerCountry.Count}");
        }

        static void TestCustomersBySpend(ICustomerSpenderRepository repository)
        {
            PrintCustomerSpenders(repository.GetAll());
        }

        static void TestFavouriteGenre(ICustomerGenreRepository repository)
        {
            PrintCustomerGenre(repository.GetById(12));
        }

        static void PrintCustomerGenre(CustomerGenre customerGenre)
        {
            StringBuilder output = new StringBuilder();
            output.Append(customerGenre.FirstName + " ");
            output.Append(customerGenre.LastName + " ");
            foreach (string genre in customerGenre.Genres)
            {
                output.Append(genre + " ");
            }

            Console.WriteLine(output.ToString());
        }

        static void PrintCustomerSpender(CustomerSpender customerSpender)
        {
            StringBuilder output = new StringBuilder();
            output.Append(customerSpender.FirstName + " ");
            output.Append(customerSpender.LastName + " ");
            output.Append(customerSpender.Spend);

            Console.WriteLine(output.ToString());
        }

        static void PrintCustomerSpenders(IEnumerable<CustomerSpender> customerSpenders)
        {
            foreach (CustomerSpender customerSpender in customerSpenders)
            {
                PrintCustomerSpender(customerSpender);
            }
        }
    }
}