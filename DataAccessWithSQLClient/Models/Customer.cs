﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessWithSQLClient.Models
{
    public readonly record struct Customer(int Id, string FirstName, string LastName, string Country, string PostalCode, string Phone, string Email);
}
