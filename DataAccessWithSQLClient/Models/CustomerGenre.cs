﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessWithSQLClient.Models
{
    internal readonly record struct CustomerGenre(int CustomerId, string FirstName, string LastName, List<string> Genres);

}
