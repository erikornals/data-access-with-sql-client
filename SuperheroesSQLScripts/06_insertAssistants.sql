﻿USE [SuperheroesDb];
GO

/*******************************************************************************
   Populate Assistant
********************************************************************************/

INSERT INTO [Assistant] ([Name], [SuperheroId]) VALUES (N'Robin', (SELECT Id FROM [Superhero] where Alias = 'Batman'));
INSERT INTO [Assistant] ([Name], [SuperheroId]) VALUES (N'Alfred', (SELECT Id FROM [Superhero] where Alias = 'Batman'));
INSERT INTO [Assistant] ([Name], [SuperheroId]) VALUES (N'Krypto', (SELECT Id FROM [Superhero] where Alias = 'Superman'));