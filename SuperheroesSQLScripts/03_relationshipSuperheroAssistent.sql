﻿USE [SuperheroesDb];
GO

/*******************************************************************************
   Create Foreign Key Assistant -> Superhero
********************************************************************************/

ALTER TABLE [Assistant] ADD CONSTRAINT [FK_AssistantSuperhero]
	FOREIGN KEY ([SuperheroId]) REFERENCES Superhero ([Id]) ON DELETE NO ACTION ON UPDATE NO ACTION;