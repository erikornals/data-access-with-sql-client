﻿USE [SuperheroesDb];
GO

--/*******************************************************************************
--   Create Tables
--********************************************************************************/
CREATE TABLE [Superhero]
(
    [Id] INT PRIMARY KEY IDENTITY(1,1),
    [Name] NVARCHAR(50) NOT NULL,
	[Alias] NVARCHAR(50) NOT NULL,
	[Origin] NVARCHAR(50) NOT NULL
);
GO
CREATE TABLE [Assistant]
(
    [Id] INT PRIMARY KEY IDENTITY(1,1),
    [Name] NVARCHAR(50) NOT NULL,
	[SuperheroId] INT
);
GO
CREATE TABLE [Power]
(
    [Id] INT PRIMARY KEY IDENTITY(1,1),
    [Name] NVARCHAR(50) NOT NULL,
	[Description] NVARCHAR(150) NOT NULL
);