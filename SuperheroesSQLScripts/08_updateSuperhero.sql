﻿USE [SuperheroesDb];
GO

/*******************************************************************************
   Update Superhero
********************************************************************************/

UPDATE [Superhero]
SET [Name] = 'Miles Morales'
WHERE [Alias] = 'Spiderman';