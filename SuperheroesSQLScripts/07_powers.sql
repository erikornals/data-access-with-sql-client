﻿USE [SuperheroesDb];
GO

/*******************************************************************************
   Populate Power
********************************************************************************/

INSERT INTO [Power] ([Name], [Description]) VALUES (N'Money', N'Being very rich');
INSERT INTO [Power] ([Name], [Description]) VALUES (N'Swinging', N'Swinging around like a monkey');
INSERT INTO [Power] ([Name], [Description]) VALUES (N'Flying', N'Flying around');
INSERT INTO [Power] ([Name], [Description]) VALUES (N'Laser eyes', N'Cold hard stare');

/*******************************************************************************
   Populate SuperheroPower
********************************************************************************/

INSERT INTO [SuperheroPower] ([SuperheroId], [PowerId]) VALUES (
	(SELECT Id FROM [Superhero] where Alias = 'Batman'), 
	(SELECT Id FROM [Power] where Name = 'Money')
);
INSERT INTO [SuperheroPower] ([SuperheroId], [PowerId]) VALUES (
	(SELECT Id FROM [Superhero] where Alias = 'Batman'), 
	(SELECT Id FROM [Power] where Name = 'Swinging')
);
INSERT INTO [SuperheroPower] ([SuperheroId], [PowerId]) VALUES (
	(SELECT Id FROM [Superhero] where Alias = 'Spiderman'), 
	(SELECT Id FROM [Power] where Name = 'Swinging')
);
INSERT INTO [SuperheroPower] ([SuperheroId], [PowerId]) VALUES (
	(SELECT Id FROM [Superhero] where Alias = 'Superman'), 
	(SELECT Id FROM [Power] where Name = 'Flying')
);
INSERT INTO [SuperheroPower] ([SuperheroId], [PowerId]) VALUES (
	(SELECT Id FROM [Superhero] where Alias = 'Superman'), 
	(SELECT Id FROM [Power] where Name = 'Laser eyes')
);