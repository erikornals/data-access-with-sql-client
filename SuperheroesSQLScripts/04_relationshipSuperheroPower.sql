﻿USE [SuperheroesDb];
GO

--/*******************************************************************************
--   Create Table
--********************************************************************************/

CREATE TABLE [SuperheroPower]
(
    [SuperheroId] INT,
	[PowerId] INT
);
GO

/*******************************************************************************
   Create Foreign Keys SuperheroPower -> Superhero, SuperheroPower -> Power
********************************************************************************/

ALTER TABLE [SuperheroPower] ADD CONSTRAINT [FK_SuperheroPowerSuperheroId]
	FOREIGN KEY ([SuperheroId]) REFERENCES [Superhero] ([Id]) ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

ALTER TABLE [SuperheroPower] ADD CONSTRAINT [FK_SuperheroPowerPowerId]
	FOREIGN KEY ([PowerId]) REFERENCES [Power] ([Id]) ON DELETE NO ACTION ON UPDATE NO ACTION;