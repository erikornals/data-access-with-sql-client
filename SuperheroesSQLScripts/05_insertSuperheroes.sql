﻿USE [SuperheroesDb];
GO

/*******************************************************************************
   Populate Superhero
********************************************************************************/

INSERT INTO [Superhero] ([Name], [Alias], [Origin]) VALUES (N'Clark Kent', N'Superman', N'Krypton');
INSERT INTO [Superhero] ([Name], [Alias], [Origin]) VALUES (N'Bruce Wayne', N'Batman', N'Gotham');
INSERT INTO [Superhero] ([Name], [Alias], [Origin]) VALUES (N'Peter Parker', N'Spiderman', N'Queens');